# trakutils

This is a repository for storing utilities and additional resources for use
by students in Aalto DSA courses. This repo is (as of now) for
(more or less experienced) CLI users.

*Note: Teaching shell newbies all the needed tricks is beyond the scope of
this repository. There are far better references freely online.
One can easily find basic Bash commands listings like
[this](http://www.tldp.org/LDP/abs/html/basic.html) or more thorough
resources like [this](http://www.gnu.org/software/bash/manual/bashref.html).
Though for Finnish speakers, we have a document containing [basic instructions](src/master/yleisohjeita.md).*


## Getting Started

Requirements:

1.  Basic Bash skills (or willingness to Google)

2.  Direct access to an Aalto Linux Desktop or a *nix system with similar
    packages installed, or a remote shell connection to such a system with
    working X11 forwarding

### Before First Usage In A File System

This is what you should do if you have never done it before in the
file system where you want to develop Trak programming exercises.

```bash
# Create a folder for all files related to the course and a subfolder for
# the zip packages
mkdir -p ~/trak/zips
# Move into the trak folder
cd ~/trak
# Clone the trak testutils repo
git clone https://smarisa@bitbucket.org/aalto-dsa/trakutils.git utils
```

### Before First Usage In A Shell Session

This is what you should do at the start of each shell session in which you
want to develop Trak programming exercises.

```bash
# Move into the trak folder
cd ~/trak
# Source the functions defined in the utils/funcs.sh file
source utils/funcs.sh
```

### Usage Example

Here is the workflow for developing a solution for the Trak Y 2014 primes
exercise (If you want to work on the first sort exercise replace `primes` with
`sort1` and if you are on the SCI version of the course you might want to work
on `sc_elevator`).

```bash
# Download the primes zip
trakget primes
# Move into the primes material folder
cd primes
# Edit the primes script
# Note: instead of the nano editor you might use vim or a graphical editor
# (eg. gedit)
nano primes.py
# Test it with the first and third IO pairs
traktest ./primes.py ./testio/0[13]
# Go back to the edit step if the script didn't work properly; Otherwise
# test it with all the IO pairs by not giving any testio directory arguments
# to the test function
# When the solution works flawlessly, open the exercise submission page with
# a graphical browser
# Note: for some reason one cannot easily authenticate with Aalto using a
# text-based browser such as lynx which would rid us from any need for
# X11 support.
firefox https://plus.cs.hut.fi/exercise/464/
# If the grader didn't give you a perfect score, you might go back to the
# editing and testing phase
```

## Notes

This repo is public and if you handle `git`, we hope you further develop it
for the benefit of your fellow students!
