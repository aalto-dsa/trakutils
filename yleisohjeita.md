# Perusohjeita

Tämä dokumentti sisältää pienen kokoelman perusohjeita ohjelmointiin liittyen.

## Sisennys

Python tulkki on tarkka sisennyksen kanssa. Sille sopii melkein mikä tahansa sisennyksen syvyys välilyönteinä (>=2) tai sitten voi käyttää tabulaattoreita, mutta eri syvyyksiä ja/tai tabulaattoreita ei kuitenkaan saa sekoittaa.

Eclipsessä voi määritellä sisennysasetuksia (tai käyttää oletuksia) ja tällöin Eclipse varoittaa epäkorrekteista sisennyksistä. Asetuksia voi muuttaa käyttöliittymästä:

``Window > Preferences > General > Appearance > Editors > Text Editors``, ja
``Window > Preferences > PyDev > Editor``

Gedit -tekstinkäsittelyohjelmassa sisennysasetuksia voi myös muuttaa:

``Edit > Preferences > Editor``

Jos määrittelee sisennykseksi 2 välilyöntiä, niin ei tarvitse muuttaa annettua koodia.


## Komentorivikäytön ABC

Tämän osion on kirjoittanut Sami Havukainen 25.9.2014.

Komentorivin käyttö on ihan hyödyllinen taito ohjelmoinnissa.
Python-kielisiäkin ohjelmia voi ihan hyvin kirjoittaa ja ajaa pelkästään komentorivin avulla.
Tässä lyhyet ohjeet Linux/Mac-komentorivin yleiskäyttöön, saa korjata jos löytyy virheitä (pahoittelut code-tägien puuttumisesta, ne on korvattu lainausmerkeillä).
Täältä voi hakea vielä lisätietoa: http://linux.fi/wiki/Komentorivin_perusteet

### Liikkuminen ja tiedostonhallinta

Varmaankin tärkein asia komentorivin käytön alussa ovat liikkuminen ja tiedostonhallinta,
joissa tärkeimmät komennot ovat ls, cd, cp, mv, mkdir ja rm.
Jos kaipaat lisätietoa jostain komennosta, käytä man-komentoa,
esim. man ls näyttää ls-komennon manuaalisivun.
ls-komento näyttää nykyisen kansion sisällön, tai sille argumenttina annetun kansion sisällön.
Hyödyllisiä parametreja ovat esimerkiksi l ja a: "ls -la" näyttää kansion tiedostot listana (l=list),
jossa näkyy tiedostonimen lisäksi esimerkiksi oikeudet, koko ja muokkaamispäivämäärä.
a-parametrin ansiosta myös piilotetut tiedostot (joiden nimi alkaa pisteellä) näkyvät listauksessa.
Ja kertauksena, komennossa: "ls -l Downloads", ls on itse komento, -l on parametri ja Downloads on argumentti.
pwd on myös hyödyllinen komento, se näyttää missä kansiossa olet.

cd komento vaihtaa yksinkertaisesti sijainnin sille argumenttina annettuun kansioon.
Sen käyttö on varsin suoraviivaista, kunhan tiedostaa mitä eroa on absoluuttisella ja suhteellisella tiedostopolulla.
Jos kotikansiossasi on vaikkapa kansio Downloads, pääsee sinne kotikansiosta komennolla "cd Downloads" (joka laajentuu absoluuttiseen muotoon "cd /home/käyttäjänimi/Downloads"), muttei komennolla "cd /Downloads", koska tämä viittaa juurikansion (/) alla sijaitsevaan Downloads-kansioon, jota luultavasti ei ole olemassakaan.
cd-komennon kirjoittaminen ilman argumentteja vie aina kotikansioon, ja lisäksi kotikansiolle on olemassa oikotie, eli ~-merkki.
Eli mistä tahansa järjestelmän kansiosta pääset aina em. Downloads-kansioon komennolla "cd ~/Downloads". "cd .." vie aina puuhierarkiassa yhtä ylempään kansioon.
Tämä johtuu siitä, että jokaisessa kansiossa on 2 piilotettua tiedostoa . ja .., joista ensimmäinen viittaa nykyiseen kansioon ja jälkimmäinen yhtä ylempään kansioon.

cp- ja mv-komennot ovat tiedostojen kopioimiseen ja siirtämiseen.
Käyttö on yksinkertaista, esim. "cp kopioitava_tiedosto kopio".
Jos kopiolla on sama nimi kuin kopioitavalla tiedostolla, nimen voi jättää kirjoittamatta,
mutta jos kopioit tiedostoa muusta kansiosta nykyiseen kansioon,
niin nykyistä kansiota pitää merkitä ./:llä. mv:tä voi käyttää myös tiedostojen uudelleennimeämiseen
(lisäksi on olemassa komento rename useampien tiedostojen uudelleennimeämiseen kerralla).
Kuten muissakin komennoissa, voi näissä käyttää argumenteissa "wildcardeja",
kuten \*-merkkiä.
Esimerkiksi komento "mv ~/Downloads/\*.mp3 ~/Music" siirtää kaikki Downloads-kansion .mp3-loppuiset tiedostot kotikansion Music-kansioon.

mkdir-komento on taas nimensä mukaisesti tarkoitettu kansioiden luomiseen, esim. "mkdir Downloads/luennot" luo luennot-nimisen kansion Downloads-kansion sisälle.
Jos haluat luoda useita sisäkkäisiä kansioita kerralla käytä mkdir:ssä -p parametria.

rm-komento on tarkoitettu tiedostojen poistamiseen.
Sen kanssa tulee olla tarkkana, koska tiedostot poistetaan kokonaan, eivätkä ne siirry siinä välissä roskakoriin.
Tyhjien kansioiden poistamiseen on olemassa komento rmdir, ja ei-tyhjiä kansioita voi poistaa rm:llä antamalla parametreiksi sekä r:n (recursive) että f:n (force).
Tämän kanssa pitääkin olla jo todella tarkkana, koska erityisesti wildcardien kanssa käytettynä edellisellä komennolla voi saada vahingossakin pahaa tuhoa aikaan.
Varoittavana esimerkkinä alla oleva linkki, joka toimikoon myös opetuksena siitä, että komentorivi on tehokas työkalu, eikä kaikkea netistä lukemaansa kannata sinne siksi tunkea :)

http://www.cultofmac.com/257976/bitcoin-hoax-dupes-apple-users-destroying-macs/

Linkin komennossa käytetään sudo-komentoa, jolla voidaan saada pääkäyttäjän oikeudet sitä seuraavaa komentoa varten. Ilman pääkäyttäjän oikeuksia vaikutusvaltasi rajoittuu lähinnä kotikansioosi, kuten esimerkiksi koulun Linux-koneilla. Päivittäisessä käytössä Linux-käyttäjät eivät joudu käyttämään sudoa oikeastaan muuten kuin ohjelmien päivittämiseen ja asentamiseen, sekä tietokoneen sammuttamiseen (mikäli nämä em. toiminnot yleensäkään tekee komentoriviltä).

### Muuta hyödyllistä

Yleisesti hyödyllistä tietoa komentorivin käyttäjälle ovat komentotulkin pikanäppäimet,
jotka oletusasetuksilla muistuttavat emacs-editorin komentoja.
Ylös- ja alasnuolilla, tai ctrl+n ja +p -näppäimillä voi selata edellisiä komentoja.
ctrl+a vie rivin alkuun ja ctrl+e loppuun.
ctrl+f selaa komentoa yhden kirjaimen verran eteenpäin ja alt+f sanan verran eteenpäin.
Sama toimii myös taaksepäin, vaihda vain f:n tilalle b, ja kirjainten/sanojen poistamiselle,
jolloin f:n tilalla on d.
Ajetut komennot tallennetaan historiaan, josta komentoja voi ajaa painamalla ctr+r ja alkamalla kirjoittaa komentoa.
Erittäin hyödyllinen ominaisuus on myös komentojen täydentäminen tabulaattorilla,
esim. kotikansiossasi luultavasti jo komennon "cd Dow" kirjoittaminen,
ja tabulaattorin painaminen saa sen laajentumaan muotoon "cd Downloads/".

Jos olet kirjoittanut komennon, jota et haluakaan ajaa, onnistuu sen keskeyttäminen painamalla ctrl+c.
Em. komennolla voit myös pysäyttää jumittavan ohjelman, tai vaihtoehtoisesti sen voi viedä taustalle komennolla ctrl+z
(ohjelman voi aloittaa suoraan taustalle lisäämällä komennon loppuun &-merkin).
Leikepöydältä voi liittää komentoriville yleensä näppäinkomennolla ctrl+shift+v,
tai hiiren keskinappia klikkaamalla, jos maalaat kopioitavan alueen ensin hiirellä
(pätee myös muualla Linuxissa).
Tiedostojen lataaminen internetistä onnistuu wget-komennolla: "wget 'url' -O haluttu_tiedostonnimi"

Lisäksi hyödyllisiä komentoja ovat cat- ja less, joilla voi tutkia tekstitiedostoja.
Jos tiedosto mahtuu kerralla ruudulle, on cat sopiva työkalu.
cat voi toimia myös yksinkertaisena editorina, jos sille ei anna argumentteja.
esim. "cat > test" -komennolla voit kirjoittaa sisältöä test-nimiseen tiedostoon EOF-signaaliin asti (ctrl+d).
less taas on tarkoitettu pidempien tiedostojen tutkimiseen, ja esim man-komento käyttää less:iä manuaalisivun näyttämiseen.

Tärkeitä komentorivin ominaisuuksia ovat myös uudelleenohjaus ja putkitus.
Esimerkiksi komento "ls > test", suorittaa ls-komennon ja ohjaa sen tulosteen test-nimiseen tiedostoon.
Jos tiedosto oli jo olemassa, sen sisältö korvautuu ls-komennon tulosteella.
Jos yksittäinen nuoli korvataan kahdella, tuloste lisätään tiedoston loppuun, eikä alkuperäinen sisältö tuhoudu: "ls >> test".
Putkitus on hyödyllinen ominaisuus, jolla voidaan ohjata komentojen tulosteita toisten komentojen syötteiksi.
Esimerkiksi komento: "ls Music/\*.mp3 | grep metallica | wc -l" laskee, montako Metallica-bändin kappaletta Music-kansiossa on.
grepin avulla voidaan poimia tulosteesta/tiedostosta sanaa tai säännöllistä lauseketta vastaavia rivejä, ja wc-komento l-parametrilla laskee rivit (wc = word count, l = lines).
Komennossa on vain yksi virhe, grep poimii vain tiedostot, joiden nimessä on metallica, eikä esim. Metallica (Linuxin tiedostojärjestelmässä kirjaimen "casella" on väliä).
Tämä voidaan korjata lisäämällä grep komennolle -i parametri (tai pidemmin kirjoitettuna --ignore-case).

### Ohjelmien ajaminen ja kirjoittaminen

Python-ohjelmia komentorivillä ajetaan yksinkertaisesti komennolla "python tiedostonnimi".
Koska tämä käy puuduttavaksi ennen pitkää, voidaan python-tiedoston alkuun lisätä ns.
shebang ("#!/usr/bin/env python"), jolloin komentotulkkimme tietää, että kyseessä on python-kielinen tiedosto.
Jos kyseessä olisi vaikka bash-kielinen skripti, olisi shebang muotoa: "#!/bin/bash".
Shebangilla varustetulle tiedostolle pitää vielä antaa suoritusoikeudet
chmod-komennolla: "chmod +x tiedosto", jonka jälkeen se voidaan ajaa komennolla "./tiedostonnimi",
mutta vain siinä kansiossa, jossa kulloinkin olet.

Voit käyttää omissa python-skripteissäkin komentoriviparametreja.
Jos sinulla on vaikkapa skripti, joka laskee tietyn bändin kappaleet musiikkikansiostasi,
olisi hienoa jos bändin nimen voisi antaa parametrina, ettei koodia joutuisi muokkaamaan
joka kerta kun päätät laskea eri bändin kappaleita.
Pythonissa komentoriviparametrit saa käyttöön importtaamalla sys-moduulin https://docs.python.org/2/library/sys.html.
Tämän jälkeen ohjelmallesi annetut parametrit tallentuvat listaan: sys.argv.
On syytä muistaa, että listan ensimmäinen alkio on itse ohjelman nimi.

Jos kyseessä on skripti, jota haluat käyttää usein, on se syytä lisätä PATH-muuttujaan,
jolloin sitä voi ajaa missä tahansa kansiossa
(ettei sinun tarvitsisi käyttää tällaisia komentoja: "Documents/Koodi/Python/laskeMetallicat.py Music/\*.mp3").
Usein tähän riittää se, että siirrät skriptin kotikansiosi bin-kansioon (tee sellainen mkdir:llä jos sitä ei ole, ja käynnistä komentorivi uudelleen).
Jos tämä ei toimi, joudut muokkaamaan komentotulkkisi config-tiedostoa, jonka tulkki käy läpi aina kun käynnistät uuden terminaalin (~/.bashrc tai ~/.zshrc, tarkista kumpaa shelliä käytät komennolla echo $SHELL).
Lisättävä rivi on: "export PATH=$PATH:/home/käyttäjänimesi/bin", juuri tässä muodossa.
Em. config-tiedoston huolimaton sorkkiminen voi johtaa tilanteeseen,
jossa edes ls-komento ei toimi, joten tiedä mitä olet tekemässä ja ota varmuuskopioita.
Jos haluat käyttää komentotulkkisi muokatun config-tiedoston käyttöön sammuttamatta terminaalia,
onnistuu se "source ~/.bashrc"-komennolla (jos siis käytät bashia).

Muita ohjelmointiin liittyviä hyödyllisiä asioita ovat varmaankin tekstieditorit sekä make-ohjelma,
joka on hyödyllinen varsinkin käännettävillä ohjelmointikielillä.
Make poistaa tarpeen kirjoittaa joka kerta esim. C-kielistä ohjelmaa kääntäessäsi "gcc -std=c99 -Wall -Werror -pedantic -o main.o main.c",
tai ohjelmaa ajettaessa "./main.o", tai debugattaessa "gdb -tui ./main.o" jne...
make on siis ohjelma, jolle voit määrittää kohteita (jotka määrität Makefilessa),
joita se suorittaa riippuen sille antamistasi argumenteista.

### Tekstieditorit

Komentorivillä ohjelmointiin käytettävistä tekstieditoreista suosituimmat ovat varmaankin
nano, vim ja emacs. Nano on kolmikosta helppokäyttöisin: ruudun alalaidassa näkyy hyödyllinen
lista pikanäppäimistä (esim. ^X = ctrl+x, poistu).
nano muistuttaa muuten toiminnallisuudeltaan lähinnä notepadia, mutta nanoon on saatavassa
esim. syntaksikorostus ja sille on monia muitakin pikanäppäimiä kuin ruudulla näkyvät.

Vim onkin jo täysin eri kaliiperin tekstieditori, jonka käytön joutuu oikeasti opettelemaan.
Vimissä on erilaisia moodeja, joista itse kirjoitusmoodi on käytössä vain silloin kun tuotat
tekstiä.
Yleisin moodi on normaalimoodi, jossa siirrytään tekstin seassa ja tehdään muutoksia lukemattomien
näppäinkomentojen avulla.
Lisäksi on olemassa 3 eri valintamoodia: yksittäisten kirjainten valitseminen
(vastaa normaalien tekstieditorien valintamoodia), rivikohtainen valinta ja
erittäin hyödyllinen block-selection, eli ns. sarakevalinta.
Aloittelijan tulee tietää vimistä ainakin seuraavat komennot:
i = siirtyy insert-moodiin (kirjoittaminen),
ESC = poistuu insert-moodista,
:w = tallentaa tiedoston (normaalimoodissa) ja
:q = lopettaa vimin (:wq = tallenna ja lopeta, :q! = lopeta tallentamatta).
Aloittelijan kannattaakin käyttää aluksi Vimin graafista versiota, gVimiä.

Emacs on haastavuudeltaan varmaankin samalla tasolla Vimin kanssa.
Komentoja on monia, ja kaikki käyttävät erilaisia valintanäppäimiä (ctrl, shift, alt, meta jne...).
Legendojen mukaan jotkin emacs-käyttäjät turvautuvat jopa jalkapedaaleihin, koska sormet
eivät meinaa riittää kaikkia komentoja varten.
Joka tapauksessa, emacs vs. vim on aihe, josta jokaisella kys. editorien käyttäjällä on oma
mielipiteensä. Lisätietoja: Yleinen vertailu wikipediassa http://en.wikipedia.org/wiki/Editor_war, jonkun tekemä vim-demo http://www.youtube.com/watch?v=pCiVCiku3cM ja jonkun toisen tekemä emacs-demo http://www.youtube.com/watch?v=EQAd41VAXWo.

On myös olemassa monia graafisia tekstieditoreja, joita voi käyttää ohjelmointiin tilanteissa,
joissa ei ole tarvetta kokonaiselle IDE:lle.
Näistä esimerkkejä ovat esim. Gedit, Notepad++, Kate ja monien ylistämä Sublime Text.
