#!/bin/bash
# This Bourne Shell file defines a few functions to make it easier to develop
# Trak programming exercise solutions using the CLI.
#
# Feel free to improve them and add commenting! Remember to make a pull
# request afterwards.

# Define some functions for internal use

function _abort()
# Print an abort message and exit with the given error code
{ echo "ERR: $2 -> Aborting..." 1>&2; exit $1; }

function exv()
# Print and execute the given arguments as a commandline and return the return
# value
{ echo "> $@"; "$@"; return $?; }

function exve()
# Print and execute the given arguments as a commandline and abort if the
# command returns a non-zero exit code
{ exv "$@"; rv=$?; [ $rv -ne 0 ] && _abort $rv 'Execution failed ('$rv')!'; }

# Define a minimalistic, but handy exercise materials package downloader
# function

function _trakget()
# Download and unzip a Trak materials package for given exercises
{
  # If no arguments were given, print usage information
  if [ $# -lt 1 ]; then
    _abort 1 "Usage: trakget EXERCISE..."
  elif [ ! -d ./zips ]; then
    _abort 2 'No ./zips directory found!'
  fi
  # Iterate over all arguments with the variable exercise
  for exercise in $@; do
    # Print an informative message
    echo "Downloading \"$exercise\"..."
    # Download the exercise zip package into the zips folder
    exve wget https://trak.cs.hut.fi/static/material/$exercise.zip -O ./zips/$exercise.zip
    # Unzip the package
    exve unzip zips/$exercise.zip
  done
  return 0
}

# Define a minimalistic, but handy programming solution tester function
# Note: Works only with IO pairs based problems.

function _traktest()
# Test a solution with IO pair directories.
{
  ## Argument Handling
  # If no arguments were given, print usage information
  if [ $# -lt 1 ]; then
    _abort 1 "Usage: traktest SOLUTION [TESTIODIR+]"
  fi
  # Define some local variables and shift the argument array by one
  local solution="$1" iodirs rv ts td; shift
  if [ ! -e "$solution" ]; then
    _abort 2 'Invalid solution "'$solution'"!'
  fi
  # If the function received more than one argument, they are saved
  # as the IO pair directories to test the solution with. Otherwise all
  # directories under ./testio will be used
  if [ $# -gt 0 ]; then iodirs="$@"
  else iodirs=./testio/*
  fi
  # Based on the file extension of the solution we determine the language
  # of the solution. Based on the language we define 1) a proper compilation
  # 2) an execution, and 3) a cleanup function.
  local fext=${solution##*.} pds="$(dirname "$solution")"
  case "$fext" in
    py)
      _compile() { exv python3.2 -c "import py_compile; py_compile.compile('$solution')"; return $?; }
      _execute() { exv python3.2 "$solution"; return $?; }
      _cleanup() { rm -r "$pds/__pycache__"; }
      ;;
    scala)
      _compile() { exv fsc "$solution"; return $?; }
      _execute() {
        local obj=$(sed -nre "s/.*object (\w+) extends App.*/\\1/p" "$solution")
        [ -z "$obj" ] && obj="$(basename "$solution")" && obj="${obj%.$fext}"
        exv scala "$obj"; return $?
      }
      _cleanup() { find . -type f -name '*.class' -delete; }
      ;;
    *)
      _abort 3 'Unsupported solution file extension ('$fext')!'
      ;;
  esac
  ## Compilation
  echo "Compiling the solution \"$solution\"..."
  ts=$(date +%s); _compile; rv=$?; td=$(date +%s); td=$(expr $td - $ts)
  [ $rv -ne 0 ] && _abort 3 'Compilation FAILED with '$rv' in '$td' seconds!'
  echo 'Compilation finished successfully in '$td' seconds.'
  ## Testing
  # Iterate over all specified IO pair directories
  for iodir in $iodirs; do
    # Print an informative message
    echo "Testing \"$solution\" with \"$iodir\"..."
    # If the directory does not contain an input file, complain and abort
    if [ ! -e "$iodir/in.txt" ]; then
      _abort 4 "No input file in \"$iodir\". Aborting..."
    fi
    # Copy the IO file pair to the current directory
    exve cp "$iodir/in.txt" "./in.txt"
    exve cp "$iodir/out.txt" "./model.txt"
    # Execute the solution with timing
    ts=$(($(date +%s%N)/1000000)); _execute; rv=$?
    td=$(($(date +%s%N)/1000000)); td=$(expr $td - $ts)
    # Notify the user of how the program finished
    echo 'The solution finished in '$td' milliseconds with code '$rv'.'
    # If the execution failed (non-zero return code) notify the user and abort
    if [ $rv -ne 0 ]; then _abort 5 'Execution failed!'; fi
    # Compare the produced output file against the model by comparing their
    # MD5 sums and proceed depending on the result
    if [ $(md5sum out.txt|cut -d' ' -f1) != $(md5sum model.txt|cut -d' ' -f1) ]; then
      # Notify the user of the fact that the outputs differ
      msg="Sorry, but the output files do not match!"; echo "$msg"
      # Display the difference using diff
      ( printf "$msg\nHere's the difference according to diff:\n--\n";
        diff ./out.txt ./model.txt ) | less
      _abort 6 'Test failed!'
    else
      # Print a success message
      echo 'And yes. The output files match!'
      # Remove the IO files
      rm in.txt out.txt model.txt
    fi
  done
  ## Cleanup
  echo 'Cleaning up...'
  _cleanup
  return 0
}

# Define a minimalistic, but handy programming solution comparison function

function _trakcmp()
# Compare the performance of solutions with an IO pair directory.
{
  ## Argument Handling
  # If no arguments were given, print usage information
  if [ $# -lt 2 ]; then
    _abort 1 "Usage: trakcomp TESTIODIR SOLUTION+"
  fi
  # Define some local variables and shift the argument array by one
  local iod=$1; shift
  if [ ! -d "$iod" ]; then
    _abort 2 'Invalid test IO directory "'$iod'"!'
  fi
  ## Gathering comparison data
  # Determine the number of test runs per solution, the temporary file and
  # some other variables
  local n=4 pftmp='.trakcmp.txt' iods='' t
  for ((i=0; i<$n; i++)); do iods="$iods $iod"; done
  # Notify the user
  echo "Performing $n test runs with each of the $# solutions using the test IO directory \"$iod\" reporting the statistics below..."
  printf "MIN\tAVG\tMAX\tSOLUTION\n"
  # Run the test function with `n` times the IO directory for each solution
  for sol in "$@"; do
    # Perform the test
    _traktest "$sol" "$iods" > "$pftmp"
    # Extract and report time usage statistics
    grep "milliseconds" "$pftmp" \
      | sed -re "s/.*in ([[:digit:]]+) millis.*/\\1/" \
      | awk 'NR == 1 { max=$1; min=$1; sum=0 }
          { if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1;}
          END {printf "%d\t%d\t%d\t'$sol'\n", min, sum/NR, max}'
    # Remove the temporary file
    rm "$pftmp"
  done
  # Return
  return 0
}

# Expose defined functions wrapped into a subshell
function trakget() { ( _trakget "$@" ); }
function traktest() { ( _traktest "$@" ); }
function trakcmp() { ( _trakcmp "$@" ); }

# EOF
